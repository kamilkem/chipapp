import { Validators} from "@angular/forms";
import { Component, OnInit } from '@angular/core';
import { Message } from '../shared/models/message.model';
import MessageService from '../shared/services/message.servise';
import { FormGroup, FormControl } from '@angular/forms';

@Component({ 
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.css']
})
export class MessageComponent implements OnInit {
 // public text: string;
  public messages: Array<Message>;
  public messageForm: FormGroup;
  public newMessage: Message;

  constructor(private messageService: MessageService) {
   // this.text = "";
   this.newMessage = new Message();
   this.newMessage.text = "";
    this.messageForm =  new FormGroup({
      text: new FormControl(this.newMessage.text),
    });
   }

  ngOnInit(): void {
    this.getAll();
  }
  get text() { return this.messageForm.get('text'); }

  public getAll(){
    this.messageService.getAll().subscribe(data => {
      this.messages = data;
    });
  }
  public saveMessage(){
   this.newMessage.text = this.text.value;
    this.messageService.save(this.newMessage).subscribe(
      result => {
        this.getAll();
        this.messageForm.reset();
      },
      error => console.error(error)
    );;

  }


}
