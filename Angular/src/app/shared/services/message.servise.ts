import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Message } from '../models/message.model';

@Injectable()
export default class MessageService {
  public API = 'https://localhost:44386/api';
  public messagesApi = `${this.API}/Messages`;

  constructor(private http: HttpClient) {}

  getAll(): Observable<Array<Message>> {
    return this.http.get<Array<Message>>(this.messagesApi);
  }

  save(message: Message): Observable<Message> {
    let result: Observable<Message>;

    return this.http.post<Message>(this.messagesApi, message);
       
  }

  getReal(text:string): Observable<string> {
    return this.http.get<string>(this.messagesApi + "/getreal", {params:{
      text: text
    }
      
    });
  }

}