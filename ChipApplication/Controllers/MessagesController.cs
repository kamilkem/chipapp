﻿using ChipApplication.Data;
using ChipApplication.Models;
using ChipApplication.Models.RequestsModels;
using NLog;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace ChipApplication.Controllers
{/// <summary>
/// Контроллер Api для взаимодействия с сущностью сообщения
/// </summary>
    public class MessagesController : ApiController
    {
        private ApplicationDbContext _dbContext;
        /// <summary>
        /// Объект для логгирования
        /// </summary>
        private Logger logger;

        public MessagesController()
        {
            _dbContext = new ApplicationDbContext();
            logger = LogManager.GetCurrentClassLogger();
        }

        // GET: api/Messages
        public async Task<List<MessageModel>> GetMessages()
      {
            try
            {
                var messages = await _dbContext.Set<Message>().OrderBy(x => x.DateTime).ToListAsync();

                var oldnewDict = await _dbContext.Set<Replacement>()
                    .ToDictionaryAsync(a => a.OldSymbol, b => b.NewSymbol);

                var response = messages.Select(x => new MessageModel(x, oldnewDict)).ToList();

                return response;
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}");
                return new List<MessageModel>();
            }

        }

        // POST: api/Messages
        [HttpPost]
        public async Task<IHttpActionResult> PostMessage(MessageModel messageModel)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            try
            {
                var message = new Message
                {
                    Text = messageModel.Text,
                    DateTime = DateTime.Now
                };

                _dbContext.Messages.Add(message);
                await _dbContext.SaveChangesAsync();

                return Ok(message);
            }
            catch (Exception ex)
            {
                logger.Error($"{ex.Message}");
                return BadRequest("Не удалось сохранить сообщение");
            }

        }
       
    }
}
