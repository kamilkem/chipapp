﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChipApplication.Models
{/// <summary>
/// Модель замены букв
/// </summary>
    public class Replacement
    {
        public int Id { get; set; }
        /// <summary>
        /// Строка, состоящая из одного символа(исходный символ)
        /// </summary>
        public string OldSymbol { get; set; }
        /// <summary>
        /// Новый символ (для шифрования)
        /// </summary>
        public string NewSymbol { get; set; }
    }
}