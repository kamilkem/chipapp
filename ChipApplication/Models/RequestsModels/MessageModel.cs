﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChipApplication.Models.RequestsModels
{/// <summary>
/// Модель сообщения для GET-запроса
/// </summary>
    public class MessageModel
    {
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Дата создания сообщения
        /// </summary>
        public string Date { get; set; }
        public MessageModel()
        {

        }
        public MessageModel(Message message, Dictionary<string, string> oldnewDict)
        {

            Text = GetChiper(message, oldnewDict);
            Date = message.DateTime.ToString("HH:mm:ss dd/MM/yyyy");

        }
        /// <summary>
        /// Метод, возвращающий зашифрованную строку
        /// </summary>
        /// <param name="message">Сущность сообщения</param>
        /// <param name="oldnewDict">Словарь, где ключ - это исходная строка, а значение - новая строка</param>
        /// <returns>Зашифрованная строка</returns>
        public string GetChiper(Message message, Dictionary<string, string> oldnewDict)
        {
            var words = message.Text.ToLower().Split(' ');
            var newwords = new List<string>();

            foreach (var word in words)
            {
                char[] oldSymbols = word.ToCharArray();
                char[] newSymbols = new char[oldSymbols.Length];
                for (int i = 0; i < oldSymbols.Length; i++)
                {
                    newSymbols[i] = oldnewDict[oldSymbols[i].ToString()][0];
                }

                newwords.Add(new string(newSymbols));
            }
            return string.Join(" ", newwords);
        }
    }
}