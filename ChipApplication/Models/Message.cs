﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ChipApplication.Models
{/// <summary>
/// Модель сообщения
/// </summary>
    public class Message
    {
        public int Id { get; set; }
        /// <summary>
        /// Текст сообщения
        /// </summary>
        public string Text { get; set; }
        /// <summary>
        /// Дата сообщения
        /// </summary>
        public DateTime DateTime { get; set; }
    }
}