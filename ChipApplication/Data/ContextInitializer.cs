﻿using ChipApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChipApplication.Data
{/// <summary>
/// Класс для инициализации базы данных
/// </summary>
    public class ContextInitializer: DropCreateDatabaseAlways<ApplicationDbContext>
    {/// <summary>
    /// Метод заполняющий таблицу замен(Replacements)
    /// </summary>
    /// <param name="db"></param>
        protected override void Seed(ApplicationDbContext db)
        {
            var alphabet = new List<string> { "а", "б","в","г","д", "е", "ё", "ж", "з", "и","й",
                "к", "л", "м", "н", "о", "п", "р", "с", "т", "у",
                "ф", "х","ц","ч","ш", "щ", "ы", "ь", "ъ", "э", "ю","я"};

            var replacedList = Replace(alphabet);

            for (int i = 0; i < alphabet.Count; i++)
            {
                var replacment = new Replacement
                {
                    OldSymbol = alphabet[i],
                    NewSymbol = replacedList[i]
                };

                db.Replacements.Add(replacment);
            }

            db.SaveChanges();
        }
        /// <summary>
        /// Метод, возращающий список, состоящий из букв русского алфавита в рандомном порядке
        /// </summary>
        /// <param name="list"></param>
        /// <returns></returns>
        private static List<string> Replace(List<string> list)
        {
            var rand = new Random();
            var replacedList = new List<string>();
            foreach (var el in list)
                replacedList.Add(el);

            for (int i = replacedList.Count - 1; i >= 0; i--)
            {
                int j = rand.Next(i + 1);
                string tmp = replacedList[j];
                replacedList[j] = replacedList[i];
                replacedList[i] = tmp;

            }
            return replacedList;
        }
    }
}