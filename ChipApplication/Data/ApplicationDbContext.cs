﻿using ChipApplication.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace ChipApplication.Data
{
    /// <summary>
    /// Класс контекста
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext() :
          base("ChiperApplicationContext")
        {
        }
        /// <summary>
        /// Статический конструктор для инициализации базы данных (его можно закомментирвать при втором запуске)
        /// </summary>
        static ApplicationDbContext()
        {
            Database.SetInitializer<ApplicationDbContext>(new ContextInitializer());
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        public DbSet<Message> Messages { get; set; }
        public DbSet<Replacement> Replacements { get; set; }
    }
}